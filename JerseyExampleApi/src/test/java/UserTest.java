import org.jerseyexample.api.core.DataManager;
import org.jerseyexample.entity.UserDO;
import org.jerseyexample.model.UserVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.function.Predicate;

/**
 * Created by warez on 24/04/17.
 */
public class UserTest {

    private DataManager dataManager;

    @Before
    public void init() {
         dataManager = new DataManager();
    }

    @Test
    public void loginTest() throws Exception {

        String email = "marco.rossi@email.it";

        Assert.assertNull(dataManager.getUsers().stream().filter(
                getUserDOByMailPredicate(email)).findFirst().get().getToken());

        dataManager.login(email, "marco");

        Assert.assertNotNull(dataManager.getUsers().stream().filter(
                getUserDOByMailPredicate(email)).findFirst().get().getToken());
    }

    @Test(expected=Exception.class)
    public void loginWrongTest() throws Exception {
        String email = "marco.rossi@email.it";

        Assert.assertNull(dataManager.getUsers().stream().filter(
                getUserDOByMailPredicate(email)).findFirst().get().getToken());

        dataManager.login(email, "wrongPassword");
    }

    @Test
    public void justLoggedTest() throws Exception {
        String email = "marco.rossi@email.it";

        String token = dataManager.getUsers().stream().filter(
                getUserDOByMailPredicate(email)).findFirst().get().getToken();

        Assert.assertNull(token);

        UserVO user = dataManager.login(email, "marco");
        token = user.getToken();
        Assert.assertNotNull(token);

        UserVO user2 = dataManager.login(email, "marco");

        Assert.assertEquals(token, user2.getToken());
    }

    private Predicate<UserDO> getUserDOByMailPredicate(String email) {
        return userDO -> userDO.getEmail().equals(email);
    }

    @Test
    public void logoutTest() throws Exception {
        String email = "marco.rossi@email.it";
        UserVO user = dataManager.login(email, "marco");

        Assert.assertNotNull(user.getToken(), "Utente " + email +" con token non null");
        String token = dataManager.getUsers().stream().filter(
                getUserDOByMailPredicate(email)).findFirst().get().getToken();

        Assert.assertEquals(user.getToken(), token);

        dataManager.logout(token);
        token = dataManager.getUsers().stream().filter(
                getUserDOByMailPredicate(email)).findFirst().get().getToken();

        Assert.assertNull(token);

    }

}
