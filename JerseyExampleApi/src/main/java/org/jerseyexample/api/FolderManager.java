package org.jerseyexample.api;

import org.jerseyexample.api.core.DataManager;
import org.jerseyexample.model.FolderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by warez on 24/04/17.
 */
@Component
public class FolderManager {

    @Autowired
    private DataManager dataManager;

    public List<FolderVO> getAll(String token) throws Exception {
        return dataManager.getAllFolderOf(token);
    }

    public FolderVO create(String token, String name) throws Exception {
        return dataManager.createFolder(token, name);
    }

    public void delete(String token, String folderId) throws Exception {
        dataManager.deleteFolder(token, folderId);
    }
}
