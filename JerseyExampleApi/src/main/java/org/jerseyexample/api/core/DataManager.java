package org.jerseyexample.api.core;

import org.jerseyexample.entity.FolderDO;
import org.jerseyexample.entity.MessageDO;
import org.jerseyexample.entity.UserDO;
import org.jerseyexample.model.FolderVO;
import org.jerseyexample.model.MessageLVO;
import org.jerseyexample.model.MessageVO;
import org.jerseyexample.model.UserVO;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;

/**
 * Created by warez on 24/04/17.
 */
@Component
@Scope(value = "singleton")
public class DataManager {

    public static final String EMAIL_SEPARATOR = ";";
    SimpleDateFormat SIMPLE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public static final String INVIATI = "Inviati";
    public static final String RICEVUTI = "Ricevuti";

    List<UserDO> users = new ArrayList<>();

    private long nextId = 100;

    public synchronized long getNextId() {
        long temp =  nextId;
        nextId++;
        return temp;
    }

    public List<UserDO> getUsers() {
        return users;
    }

    public DataManager() {

        initUser();
        initFolder();
    }

    private void initFolder() {

        for(UserDO u: users) {
            u.getFolders().add(createFolderCore(INVIATI, false));
            u.getFolders().add(createFolderCore(RICEVUTI, false));
        }
    }

    private FolderDO createFolderCore(String name, boolean cancellable) {
        FolderDO f = new FolderDO();
        f.setId( getNextId() );
        f.setNome(name);
        f.setCancellable(cancellable);
        return f;
    }

    private void initUser() {
        List<String> names = Arrays.asList("Marco", "Alessandro", "Alessia");
        List<String> surnames = Arrays.asList("Rossi", "Bianchi", "Viola");
        List<String> email = Arrays.asList("marco.rossi@email.it", "aleale@email.it", "alexViola@email.it");
        List<String> pass = Arrays.asList("marco", "ale", "viola");

        for(int i = 0; i < names.size(); i++) {
            UserDO u = new UserDO();
            u.setNome(names.get(i));
            u.setCognome(surnames.get(i));
            u.setEmail(email.get(i));
            u.setPassword(pass.get(i));
            u.setId(getNextId());

            users.add(u);
        }
    }


    public List<FolderVO> getAllFolderOf(String token) throws Exception {
        UserDO first = getUserDOFromToken(token);

        List<FolderDO> folders = first.getFolders();
        return convertFoldersToVO(folders);
    }

    private UserDO getUserDOFromToken(String token) throws Exception {
        Optional<UserDO> first = users.stream().filter(userDO -> userDO.getToken() != null && userDO.getToken().equals(token)).findFirst();
        if (!first.isPresent())
            throw new Exception("Utente con token " + token + " non trovato");
        return first.get();
    }

    private UserDO getUserDOFromEmail(String email) throws Exception {
        Optional<UserDO> first = users.stream().filter(userDO -> userDO.getEmail().equals(email)).findFirst();
        if (!first.isPresent())
            throw new Exception("Utente con email " + email + " non trovato");
        return first.get();
    }

    private List<FolderVO> convertFoldersToVO(List<FolderDO> folders) {
        List<FolderVO> ret = new ArrayList<>();
        for(FolderDO f: folders)
            ret.add( convertFolderToVo(f)) ;
        return ret;
    }

    private FolderVO convertFolderToVo(FolderDO f) {
        FolderVO ret = new FolderVO();
        ret.setId("" + f.getId());
        ret.setNome( f.getNome() );
        return ret;
    }

    public FolderVO createFolder(String token, String name) throws Exception {
        UserDO first = getUserDOFromToken(token);
        List<FolderDO> folders = first.getFolders();

        if( folders.stream().filter(folderDO -> folderDO.getNome().equals(name)).count() > 0 )
            throw new Exception("La cartella con nome " + name + " è  già presente");

        FolderDO newFolder = createFolderCore(name, true);
        folders.add(newFolder);

        return convertFolderToVo(newFolder);

    }

    public void deleteFolder(String token, String folderId) throws Exception {
        UserDO first = getUserDOFromToken(token);
        List<FolderDO> folders = first.getFolders();

        FolderDO folderDO = getFolderDOFromName(folderId, folders);

        if(!folderDO.isCancellable())
            throw new Exception("La cartella " + folderDO.getNome() + " non può essere cancellata");

        folders.remove(folderDO);
    }

    private FolderDO getFolderDOFromName(String folderName, List<FolderDO> folders) throws Exception {
        Predicate<FolderDO> folderDOPredicate = folderDO -> folderDO.getNome().equals(folderName);
        if( !folders.stream().anyMatch(folderDOPredicate))
            throw new Exception("La cartella con nome " + folderName + " non esiste");

        return folders.stream().filter(folderDOPredicate).findFirst().get();
    }

    public <T extends MessageLVO> T sendMessage(String token, MessageVO message) throws Exception {
        UserDO first = getUserDOFromToken(token);

        List<FolderDO> folders = first.getFolders();
        FolderDO folderDO = getFolderDOFromName(INVIATI, folders);

        message.setDate(SIMPLE_FORMAT.format(new Date()));
        MessageDO m = convertToMessageDO(message);
        T ret = convertMessageToVO(m, true);

        String[] toList = m.getTo().split(EMAIL_SEPARATOR);
        for(String userMail: toList) {
            UserDO u = getUserDOFromEmail(userMail);
            List<FolderDO> folders1 = u.getFolders();
            FolderDO ricevutiFolder = getFolderDOFromName(RICEVUTI, folders1);
            ricevutiFolder.getMessages().add(m);
        }

        folderDO.getMessages().add(m);
        return ret;
    }

    private MessageDO convertToMessageDO(MessageVO message) throws Exception {
        MessageDO ret = new MessageDO();
        ret.setBody(message.getBody());
        ret.setDate( SIMPLE_FORMAT.parse(message.getDate()) );
        ret.setId( message.getId() == null ? getNextId() : Long.parseLong(message.getId()) );
        ret.setOggetto(message.getOggetto());
        ret.setTo(message.getTo());
        return ret;
    }

    public <T extends MessageLVO> T getMessage(String token, String messageId, boolean light) throws Exception {
        T messageDO = findMessageById(token, messageId, light);
        if (messageDO == null)
            throw new Exception("Messaggio con id " + messageId + " non tovato");

        return messageDO;
    }

    public List<MessageLVO> getMessageInFolder(String token, String folderId, boolean light) throws Exception {
        UserDO first = getUserDOFromToken(token);
        List<FolderDO> folders = first.getFolders();

        FolderDO folderDO = getFolderDOFromName(folderId, folders);
        List<MessageDO> messages = folderDO.getMessages();

        List<MessageLVO> converted = convertMessagesToVO(messages, light);
        return converted;

    }

    private List<MessageLVO> convertMessagesToVO(List<MessageDO> messages, boolean light) {
        List<MessageLVO> ret = new ArrayList<>();
        for(MessageDO m : messages)
            ret.add( convertMessageToVO(m, light) );

        return ret;
    }

    private <T extends MessageLVO> T convertMessageToVO(MessageDO m, boolean light) {
        MessageLVO ret = light ? new MessageLVO() : new MessageVO();

        ret.setId( "" + m.getId() );
        ret.setDate(SIMPLE_FORMAT.format(m.getDate()) );
        ret.setOggetto(m.getOggetto());
        ret.setTo(m.getTo());

        if(!light) {
            ((MessageVO)ret).setBody(m.getBody());
        }

        return (T)ret;
    }

    public MessageLVO deleteMessage(String token, String messageId) throws Exception {
        MessageLVO messageDO = findMessageById(token, messageId, true);
        if (messageDO != null)
            return messageDO;

        throw new Exception("Messaggio con id " + messageId + " non tovato");
    }

    private <T extends MessageLVO> T findMessageById(String token, String messageId, boolean light) throws Exception {
        UserDO first = getUserDOFromToken(token);

        List<FolderDO> folders = first.getFolders();
        for(FolderDO f: folders) {
            List<MessageDO> messages = f.getMessages();
            Optional<MessageDO> first1 = messages.stream().filter(messageDO -> messageDO.getId() == Long.parseLong(messageId)).findFirst();
            if(!first1.isPresent())
                continue;

            MessageDO messageDO = first1.get();
            messages.remove(messageDO);

            return convertMessageToVO(messageDO, light);
        }

        return null;
    }

    public UserVO login(String email, String password) throws Exception {

        UserDO first;

        try {
            first = getUserDOFromEmail(email);
        } catch (Exception e) {
            throw new Exception("User o password errati");
        }

        if(!first.getPassword().equals(password))
            throw new Exception("User o password errati");

        if(first.getToken() == null)
            first.setToken(UUID.randomUUID().toString());

        return convertUserToVO(first);

    }

    public UserVO logout(String token) throws Exception {
        UserDO first = getUserDOFromToken(token);
        first.setToken(null);

        UserVO user = convertUserToVO(first);
        return user;
    }

    private UserVO convertUserToVO(UserDO first) {
        UserVO ret = new UserVO();
        ret.setCognome(first.getCognome());
        ret.setNome(first.getNome());
        ret.setEmail(first.getEmail());
        ret.setToken(first.getToken());
        return ret;
    }
}
