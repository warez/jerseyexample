package org.jerseyexample.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MDONATI on 21/04/2017.
 */
public class FolderDO {

    private String nome;

    private long id;

    private boolean cancellable;

    private List<MessageDO> messages = new ArrayList<>();

    public List<MessageDO> getMessages() {
        return messages;
    }

    public boolean isCancellable() {
        return cancellable;
    }

    public void setCancellable(boolean cancellable) {
        this.cancellable = cancellable;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "FolderDO{" +
                "nome='" + nome + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
