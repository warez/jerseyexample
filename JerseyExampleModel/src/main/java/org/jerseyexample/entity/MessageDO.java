package org.jerseyexample.entity;

import java.util.Date;

/**
 * Created by warez on 24/04/17.
 */
public class MessageDO {

    private long id;

    private String oggetto;

    private String to;

    private Date date;

    private String body;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOggetto() {
        return oggetto;
    }

    public void setOggetto(String oggetto) {
        this.oggetto = oggetto;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "MessageDO{" +
                "id='" + id + '\'' +
                ", oggetto='" + oggetto + '\'' +
                ", to='" + to + '\'' +
                ", date=" + date +
                ", body='" + body + '\'' +
                '}';
    }
}
