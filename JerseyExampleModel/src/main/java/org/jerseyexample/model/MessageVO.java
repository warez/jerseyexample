package org.jerseyexample.model;

/**
 * Created by warez on 24/04/17.
 */
public class MessageVO extends MessageLVO {

    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "MessageVO{" +
                " body='" + body + '\'' +
                "} " + super.toString();
    }
}
