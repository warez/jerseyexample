package org.jerseyexample.model;

/**
 * Created by warez on 24/04/17.
 */
public class MessageLVO {

    private String id;

    private String oggetto;

    private String to;

    private String date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOggetto() {
        return oggetto;
    }

    public void setOggetto(String oggetto) {
        this.oggetto = oggetto;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "MessageLVO{" +
                "id='" + id + '\'' +
                ", oggetto='" + oggetto + '\'' +
                ", to='" + to + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
