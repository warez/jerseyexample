package org.jerseyexample.rs.fatto;

import org.springframework.stereotype.Component;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import java.util.List;

/**
 * Created by warez on 24/04/17.
 */
@Component
public class AbstractResource {

    @Context
    private HttpHeaders headers;

    protected String getToken() {
        if(headers == null)
            return null;

        List<String> requestHeader = headers.getRequestHeader("X-TOKEN");
        if(requestHeader == null || requestHeader.size() == 0)
            return null;

        return requestHeader.get(0);
    }
}
